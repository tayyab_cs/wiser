<?php

/**
 * The Controller for Static Pages.
 *
 * @category Controller
 * @package  Wiser/app
 * @author   Muhammad Tayyab <mtbahauddin@gmail.com>
 * @license  https://opensource.org/licenses/MIT MIT Licencse
 * @link     http://wiser/home OR http://wiser/about
 */
class Pages extends CI_Controller
{

    /**
     * Render the static page view.
     *
     * @param string $page The HTTP argument to the Pages controller.
     *
     * @return null
     */
    public function view($page = 'home')
    {
        if (!file_exists(APPPATH . 'views/pages/' . $page . '.php')) {
            show_404();
        }

        $data['title'] = ucfirst($page);

        $this->load->view('templates/header');
        $this->load->view('pages/' . $page, $data);
        $this->load->view('templates/footer');
    }
}
