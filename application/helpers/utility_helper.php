<?php

/**
 * The Helper Function to get Assets_url
 *
 * @return string URL to the assets folder of website.
 */
function asset_url()
{
    return base_url() . 'assets/';
}
